from PyPDF2 import PdfReader


def text_extract(file):
    reader = PdfReader(file)
    text = ""
    for page in reader.pages:
        text += page.extract_text() + "\n"
    text = text.replace("\n", " ")
    while "  " in text:
        text = text.replace("  ", " ")

    string_encode = text.encode("ascii", "ignore")
    string_decode = string_encode.decode()

    return string_decode
