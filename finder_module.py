import time

import torch
from models import model
from sentence_transformers.util import cos_sim
from fuzzywuzzy import process
from config import threshold, fuzzy_threshold

skills_emb = torch.load("bot_fixed_entities_embeddings.pt")
task_emb = torch.load("bot_task_embeddings.pt")
tech_emb = torch.load("bot_tech_skills_embeddings.pt")
jobs_emb = torch.load("bot_titles_embeddings.pt")
tools_emb = torch.load("bot_tools_embeddings.pt")


def get_fuzzy(ins, names, descr=None, thresh=None):
    if thresh is None:
        thresh = fuzzy_threshold
    if descr is None:
        found = process.extract(ins, names, limit=1)
        if found[0][1] >= thresh:
            data = found[0]
            result = [data[1] / 100, data[0], ""]
            return result

    elif isinstance(ins, str) and len(names) == len(descr):
        found = process.extract(ins, names, limit=1)
        found1 = process.extract(ins, descr, limit=1)
        if found[0][1] >= thresh and found[0][1] > found1[0][1]:
            data = found[0]
            result = [data[1] / 100, data[0], descr[names.index(data[0])]]
            return result
        elif found1[0][1] >= thresh:
            data = found1[0]
            result = [data[1] / 100, names[descr.index(data[0])], data[0]]
            return result
    return []


def get_similar(names, emb_question, emb_names, descr=None, emb_descr=None):
    result = []
    scores = cos_sim(emb_question, emb_names)
    maximus = torch.max(scores, 1)
    m = float(maximus.values[0])
    i = int(maximus.indices[0])
    if m > threshold:
        result = [round(m, 4), names[i], descr[i] if descr is not None else ""]
    if descr is not None and emb_descr is not None and len(names) == len(descr):
        scores2 = cos_sim(emb_question, emb_descr)
        maximus2 = torch.max(scores2, 1)
        m2 = float(maximus2.values[0])
        i2 = int(maximus2.indices[0])
        if m2 > m and m2 > threshold:
            result = [round(m2, 4), names[i2], descr[i2]]
    return result


def get_result(result, entity, en_type):
    a = ["abilities", "knowledge", "skills", "work_activities"]
    if result.get("score") < entity[0]:
        result["score"] = entity[0]
        result["entity"] = en_type
        result["name"] = entity[1]
        if en_type in a:
            result["descr"] = entity[2]
        elif result.get("descr") is not None:
            del result["descr"]
    return result


def find(ins):
    emb_question = model.encode(ins)
    result = {"original_input": ins, "entity": "", "score": 0}
    for key in skills_emb:
        entities = skills_emb.get(key)
        names = entities.get("names")
        descr = entities.get("descr")
        emb_names = entities.get("emb_names")
        emb_descr = entities.get("emb_descr")

        similar = get_similar(names, emb_question, emb_names, descr, emb_descr)
        if len(similar) > 0:
            result = get_result(result, similar, key)

    tasks = task_emb.get("tasks")
    emb_tasks = task_emb.get("embeddings")
    task_result = get_similar(tasks, emb_question, emb_tasks)
    if len(task_result) > 0:
        result = get_result(result, task_result, "tasks")

    tech = tech_emb.get("tech_skills")
    emb_tech = tech_emb.get("embeddings")
    tech_result = get_similar(tech, emb_question, emb_tech)
    if len(tech_result) > 0:
        result = get_result(result, tech_result, "tech_skills")

    tools = tools_emb.get("tools")
    emb_tools = tools_emb.get("embeddings")
    tools_result = get_similar(tools, emb_question, emb_tools)
    if len(tools_result) > 0:
        result = get_result(result, tools_result, "tools")

    jobs = jobs_emb.get("titles")
    codes = jobs_emb.get("codes")
    emb_jobs = jobs_emb.get("emb_titles")
    jobs_result = get_similar(jobs, emb_question, emb_jobs)
    if len(jobs_result) > 0:
        result = get_result(result, jobs_result, "jobs")
        if result.get("entity") == "jobs":
            result["job_code"] = codes[jobs.index(jobs_result[1])]
            indices = [i for i, x in enumerate(codes) if x == result["job_code"]]
            result["alternative_names"] = [jobs[x] for x in indices if x < len(jobs)]

    # if result.get("score") == 0:
    #     result = find_fuzzy(ins)
    return result


def find_fuzzy(ins):
    result = {"original_input": ins, "entity": "", "score": 0}
    for key in skills_emb:
        entities = skills_emb.get(key)
        names = entities.get("names")
        descr = entities.get("descr")

        fuzzy = get_fuzzy(ins, names, descr)
        if len(fuzzy) > 0:
            result = get_result(result, fuzzy, key)

    tasks = task_emb.get("tasks")
    task_result = get_fuzzy(ins, tasks)
    if len(task_result) > 0:
        result = get_result(result, task_result, "tasks")

    tech = tech_emb.get("tech_skills")
    tech_result = get_fuzzy(ins, tech)
    if len(tech_result) > 0:
        result = get_result(result, tech_result, "tech_skills")

    tools = tools_emb.get("tools")
    tools_result = get_fuzzy(ins, tools)
    if len(tools_result) > 0:
        result = get_result(result, tools_result, "tools")

    jobs = jobs_emb.get("titles")
    codes = jobs_emb.get("codes")
    jobs_result = get_fuzzy(ins, jobs)
    if len(jobs_result) > 0:
        result = get_result(result, jobs_result, "jobs")
        if result.get("entity") == "jobs":
            result["job_code"] = codes[jobs.index(jobs_result[1])]
            indices = [i for i, x in enumerate(codes) if x == result["job_code"]]
            result["alternative_names"] = [jobs[x] for x in indices if x < len(jobs)]

    return result


def main():
    question = ""
    while question != 'exit':
        question = input('Question ')
        if question == 'exit':
            break
        total_time = time.time() * 1000
        data = find(question)
        print("Total time ", time.time() * 1000 - total_time)
        print(data)


if __name__ == "__main__":
    main()
