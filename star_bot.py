import config
from open_domain_module import open_domain
import grammar_module
import models
import spc_parser
import language
from function_factory import functions, default_query_type


class Starbot:
    def __init__(self):
        self.grammar = grammar_module.Grammar(language.starbot_grammar, language.free_questions)

    @staticmethod
    def get_answer(data):
        selected_question = data.get("selected_question")
        if selected_question is None or len(selected_question) == 0:
            original_input = " ".join(data.get("sentences"))
            if original_input == "":
                data["answer"] = "There is no question!"
                data["question_type"] = "98"
                return data
            data["answer"], data["url"] = open_domain(original_input)
            data["question_type"] = "99"
            return data
        score = selected_question[0]
        selected_question = selected_question[1]
        if score < config.threshold:
            data["answer"], data["url"] = open_domain(data.get("question"))
            return data
        question_type = int(selected_question[:2])
        data["question_type"] = selected_question[:2]
        ans_fun = default_query_type if question_type >= len(functions) else functions[question_type]
        data = ans_fun(data)
        return data

    def cycle(self, question):
        data = spc_parser.analyze(question)
        data["selected_question"] = models.get_similar(question, self.grammar.complete_questions(
            data) + self.grammar.free_questions)
        data = self.get_answer(data)
        return data


def main():
    starbot = Starbot()
    question = ""
    while question != 'exit':
        question = input('User: ')
        if question == 'exit':
            break
        if question == "":
            question = "Hello there..."
        data = starbot.cycle(question)
        answer = data.get("answer")
        question_type = data.get("question_type")
        print(data)
        print("\n\nSTAR-bot:", question_type, answer)


if __name__ == "__main__":
    main()
