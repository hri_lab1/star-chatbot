# import json
import re
import time
import spacy
from words2num import w2n
import language
from finder_module import find
nlp = spacy.load("en_core_web_sm")


def get_np(query):
    doc = nlp(query)
    return [chunk.text for chunk in doc.noun_chunks]


def get_sentences(query):
    doc = nlp(query)
    return [x.text for x in doc.sents]


def analyze(query):
    doc = nlp(query)
    data = {
        "sentences": [x.text for x in doc.sents],
        "nouns": [token.text for token in doc if token.pos_ == "NOUN" or token.pos_ == "PROPN"],
        "nouns_phrases": [chunk.text for chunk in doc.noun_chunks],
        "verbs": [token.lemma_ for token in doc if token.pos_ == "VERB"],
        "numbers": [],
        "numbers2": [],
        "all_numbers": [token.lemma_ for token in doc if token.pos_ == "NUM"],
        "entities": [{"entity": entity.label_,
                      "word": entity.text,
                      "start": entity.start,
                      "end": entity.end
                      } for entity in doc.ents]
    }
    data["dates"] = [x for x in data["entities"] if x["entity"] == "DATE"]

    qlist = re.findall('\"(.*?)\"', query)
    data["quoted"] = qlist

    for ent in data["entities"]:
        if ent.get("entity") == "CARDINAL" and not ent.get("word").isnumeric():
            num = w2n(ent.get("word"))
            data["numbers"].append(str(num))
        elif ent.get("entity") == "CARDINAL":
            data["numbers"].append(ent.get("word"))

    numbers = [x["word"].split(" ") for x in data["entities"] if x["entity"] == "DATE"]
    numbers2 = []
    for n2 in numbers:
        number = ""
        for nw in n2:
            if nw in data["all_numbers"]:
                number += " " + nw
        if len(number) > 0:
            numbers2.append(number.strip())

    for num in numbers2:
        if not num.isnumeric():
            num = w2n(num)
        data["numbers2"].append(str(num))

    entities = []
    for ent in data["entities"]:
        if ent.get("entity") != "CARDINAL" and ent.get("entity") != "DATE":
            entities.append(ent)

    data["entities"] = entities

    # for token in doc:
    #     print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
    #           token.shape_, token.is_alpha, token.is_stop)
    # print(data)

    data = find_valid_entities(data)
    return data


def find_valid_entities(data):
    # get list of NER entities
    en = []
    ins = []
    if len(data['entities']) > 0:
        for entity in data['entities']:
            en.append(entity['word'].lower())
            ins.append(entity['word'])

    if len(data['dates']) > 0:
        for d in data['dates']:
            en.append(d["word"].lower())

    # check noun_phrases for keywords
    if len(data['nouns_phrases']) > 0:
        nouns_phrases = []
        for nouns_phrase in data['nouns_phrases']:
            new_nouns_phrase = []
            ws = nouns_phrase.split(" ")
            for w in ws:
                if w.lower() not in " ".join(language.key_words) and w.lower() not in data.get("all_numbers"):
                    new_nouns_phrase.append(w)
            nouns_phrases.append(" ".join(new_nouns_phrase))

        verified_nouns_phrases = []

        for np in nouns_phrases:
            if np in get_np(np):
                verified_nouns_phrases.append(np)

        data['nouns_phrases'] = verified_nouns_phrases

    # check nouns for keywords
    if len(data['nouns']) > 0:
        nouns = []
        for noun in data['nouns']:
            if noun.lower() not in " ".join(language.key_words):
                nouns.append(noun)
        data['nouns'] = nouns

    # check noun_phrases for entities
    if len(data['nouns_phrases']) > 0 and len(data['entities']) > 0:
        nouns_phrases = []
        for word in data['nouns_phrases']:
            np = word.lower().split(" ")

            new_np = ""
            for wnp in np:
                if wnp not in " ".join(en):
                    new_np += " " + wnp
            new_np = new_np.strip()
            if len(new_np) > 0:
                nouns_phrases.append(new_np)

        verified_nouns_phrases = []

        for np in nouns_phrases:
            if np in get_np(np):
                verified_nouns_phrases.append(np)

        data['nouns_phrases'] = verified_nouns_phrases

    # check nouns for noun_phrases
    if len(data['nouns']) > 0:
        nouns = []
        for noun in data["nouns"]:
            if noun not in " ".join(data["nouns_phrases"]):
                nouns.append(noun)
        data['nouns'] = nouns

    # check nouns for entities
    if len(data['nouns']) > 0:
        nouns = []
        for word in data['nouns']:
            if word.lower() not in " ".join(en):
                nouns.append(word)
        data["nouns"] = nouns

    ins += data["nouns"] + data["nouns_phrases"]

    # new_ins = []
    # for i in ins:
    #     sep_ins = i.split(" and ")
    #     new_ins += sep_ins
    # ins = new_ins

    sentence = data.get("sentences")[0]
    to_ord_instances = []
    for i in ins:
        if i in sentence:
            ind = sentence.index(i)
        else:
            ind = 0
        to_ord_instances.append([ind, i])
    sorted_instances = sorted(to_ord_instances, key=lambda x: x[0], reverse=False)
    ordered_ins = [x[1] for x in sorted_instances]

    verified_ordered_ins = []
    for np in ordered_ins:
        if np not in language.residual_key_words:
            verified_ordered_ins.append(np)

    data['ins'] = verified_ordered_ins

    instances = []

    for q in data.get("quoted"):
        new_instance = find(q)
        if new_instance.get("entity") != "":
            instances.append(new_instance)

    quoted_text_list = [x.get("original_input") for x in instances]
    quoted_text = " ".join(quoted_text_list).lower()
    not_quoted_text = " ".join(data.get("sentences")).lower()

    for t in quoted_text_list:
        not_quoted_text = not_quoted_text.replace('"'+t.lower()+'"', "")

    new_ins_list = []
    for np in data.get("ins"):
        np = np.replace('"', '')
        if (np not in quoted_text or (np in not_quoted_text)) and np not in new_ins_list:
            new_ins_list.append(np)
    data['ins'] = new_ins_list

    for np in data.get("ins"):
        new_instance = find(np)
        if new_instance.get("entity") != "" and new_instance not in instances:
            instances.append(new_instance)
    data["instances"] = instances

    return data


def main():
    question = ""
    while question != 'exit':
        question = input('Question ')
        if question == 'exit':
            break
        total_time = time.time() * 1000
        data = analyze(question)
        print("Total time ", time.time() * 1000 - total_time)
        print(data)


if __name__ == "__main__":
    main()
