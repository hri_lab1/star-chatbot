# import inflect
# en = inflect.engine()
# print(en.singular_noun("utilities"))
import time
import models
import language
from spc_parser import analyze
from open_domain_module import open_domain


class Grammar:
    def __init__(self, grammar, free_questions=None):
        self.grammar = grammar
        self.threshold = 0.75
        self.free_questions = [] if free_questions is None else [key for key in free_questions]
        self.questions = list(set(self.expand()))

    # extracts the list of possible utterances from the grammar
    def expand(self, grammar=None, g_dict=None):
        if g_dict is None:
            g_dict = {}
        if grammar is None:
            grammar = self.grammar
        grammar = grammar.replace('\n', '')
        ph_list = grammar.split(';')
        ph_list.pop(0)
        ph_list[0] = ph_list[0].replace(' grammar ', '', 1)
        g = ph_list.pop(0)
        g_dict[g] = {}

        for i in range(len(ph_list)):
            ph_list[i] = ph_list[i].strip()
            ph_list[i] = ph_list[i].split('=')

            for k in range(len(ph_list[i])):
                ph_list[i][k] = ph_list[i][k].strip()

            n = ph_list[i][0].find('<') + 1
            n2 = ph_list[i][0].find('>')
            ph_list[i][0] = ph_list[i][0][n: n2]

            if len(ph_list[i]) == 1 and ph_list[i][0] == '':
                del ph_list[i]

            if ph_list[i] and ph_list[i][1] and ph_list[i][1].find('|') != -1:
                ph_list[i][1] = ph_list[i][1].split('|')

                for j in range(len(ph_list[i][1])):
                    ph_list[i][1][j] = ph_list[i][1][j].strip()

            if ph_list[i] is not None and isinstance(ph_list[i][1], list):
                g_dict[g][ph_list[i][0]] = ph_list[i][1]
            elif ph_list[i] is not None:
                g_dict[g][ph_list[i][0]] = [ph_list[i][1]]

        for key in g_dict[g]:
            while self.verify_content(g_dict[g][key]):
                for i in range(len(g_dict[g][key])):
                    n = g_dict[g][key][i].find('<') + 1
                    n2 = g_dict[g][key][i].find('>')
                    if n > 0:
                        k = g_dict[g][key][i][n: n2]
                        if g_dict[g][k] is not None:
                            for s in range(len(g_dict[g][k])):
                                g_dict[g][key].append(g_dict[g][key][i].replace('<' + k + '>', g_dict[g][k][s]))

                        del g_dict[g][key][i]

        result = g_dict[g][[x for x in g_dict[g]][-1]]
        for n in range(len(result)):
            result[n] = result[n].replace('  ', ' ')
        distinct_result = []
        for q in result:
            if q not in distinct_result:
                distinct_result.append(q)
        return distinct_result

    # check if there are words between <>
    @staticmethod
    def verify_content(v_list):
        for k1 in range(len(v_list)):
            if '<' in v_list[k1]:
                return True
        return False

    def complete_questions(self, data):
        jobs = [x.get("name") for x in data.get("instances") if x.get("entity") == "jobs"]
        j = len(jobs)
        j0 = "{jobs[0]}"
        j1 = "{jobs[1]}"
        entities = [x for x in data.get("instances") if x.get("entity") != "jobs" and x.get("entity") != ""]
        e = len(entities)
        e0 = "{entity"
        num = (data.get("numbers"))
        n = len(num)
        all_ = [x.get("name") for x in data.get("instances") if x.get("name") is not None]
        a = len(all_)
        a0 = "{all}"

        new_questions = []
        for q in self.questions:
            if j == 1 and e == 0 and a0 not in q:
                if j0 in q and j1 not in q and e0 not in q:
                    new_questions.append(q.format(jobs=jobs, num="" if n == 0 else num[0]))
            elif a >= 1 and a0 in q and j1 not in q:
                for ent in all_:
                    new_questions.append(q.format(all=ent))
            elif j == 2 and e == 0 and a0 not in q:
                if j0 in q and j1 in q and e0 not in q:
                    new_questions.append(q.format(jobs=jobs, num="" if n == 0 else num[0]))
            elif j == 0 and e >= 1 and a0 not in q and j0 not in q and j1 not in q:
                new_questions.append(q.format(**entities[0], num="" if n == 0 else num[0]))
        return new_questions + self.free_questions


def main():
    question = ""
    grammar = Grammar(language.starbot_grammar, language.free_questions)

    while question != 'exit':
        question = input('Question ')
        if question == 'exit':
            break
        if question == "":
            question = "Hello there..."
        data = analyze(question)
        print("data:", data)
        questions = grammar.complete_questions(data)
        total_time = time.time() * 1000
        a = models.get_similar_list(question, questions, 5)
        print("List time ", time.time() * 1000 - total_time)
        total_time = time.time() * 1000
        b = models.get_similar(question, questions)
        print("Single time ", time.time() * 1000 - total_time)
        print(a, b)
        if a[0][0] > 0.75:
            print(a[0][1])
        else:
            open_domain(question)


if __name__ == "__main__":
    main()



