# import json
import time

# import requests
from duckduckgo_search import ddg
from duckduckgo_search import DDGS

# import html_text
from models import *
from config import threshold
from spc_parser import get_sentences
from itertools import islice

ddgs = DDGS()

def open_domain(query):
    answer = "Sorry, I didn't find anything about it!"
    url = ""
    try:
        answer, url = duckduckgo(query)
    except:
        if len(query) > 2:
            query = query[:-1]
            try:
                answer, url = duckduckgo(query)
            except:
                pass
    return answer, url


def duckduckgo(query):
    # r = ddg(query, region='wt-wt', safesearch='On', max_results=10)  # time='y'
    r = ddgs.text(query, region='wt-wt', safesearch='on')

    if r is None:
        return "Sorry, I didn't find anything about it!"

    res = []
    for a in islice(r, 10):
        res.append(a)


    texts = [x.get('title') + " - " + x.get('body') for x in res]
    # text = " - ".join(texts)

    results = []
    for i, t in enumerate(texts):
        result = question_answerer(question=query, context=t)
        result["text"] = res[i].get("title") + " - " + res[i].get("body")
        result["url"] = res[i].get("href")
        results.append(result)
    sorted_results = sorted(results, key=lambda x: x["score"], reverse=True)

    # for result in sorted_results:
    #     print(result["score"], result["answer"])

    answers = [x.get("answer") for x in sorted_results]
    emb_ans = model.encode(answers)
    scores = cos_sim(emb_ans, emb_ans)
    # print(scores)

    for i, row_score in enumerate(scores):
        acc = 0
        acc_text = ""
        for j, score in enumerate(row_score):
            if score.item() > threshold and j >= i:
                acc += sorted_results[j].get("score")
                acc_text += sorted_results[j].get("text") + " - "
        sorted_results[i]["sum_score"] = acc
        sorted_results[i]["txt"] = acc_text
    sorted_results = sorted(sorted_results, key=lambda x: x["sum_score"], reverse=True)

    # for result in sorted_results:
    #     print(result)

    answer = ""
    text = sorted_results[0]["txt"]
    url = sorted_results[0].get("url")
    if sorted_results[0]["score"] > threshold:
        # print(sorted_results[0].get("answer"))
        answer += sorted_results[0].get("answer") + " - "

    text = text[:2500]
    max_length = 150 if len(text) / 4 > 150 else int(len(text) / 4.5)
    min_length = 100 if len(text) / 4 > 150 else int(len(text) / 6.5)
    # print("max:", max_length, "min:", min_length, "len:", len(text))
    txt = summarizer(text, max_length=max_length, min_length=min_length, do_sample=False)[0]['summary_text']
    txt = " ".join(get_sentences(txt)[:4]).replace(" .", ". ").replace("  ", " ")
    # print(txt)
    answer += txt
    return answer, url


def main():
    question = ""
    while question != 'exit':
        question = input('Question ')
        if question == 'exit':
            break
        total_time = time.time() * 1000
        print(open_domain(question))
        print("Total time ", time.time() * 1000 - total_time)


if __name__ == "__main__":
    main()
# who wins the last 100m European Championship
