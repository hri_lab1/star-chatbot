import config
from transformers import pipeline
import torch
from sentence_transformers import SentenceTransformer
from sentence_transformers.util import cos_sim

model = SentenceTransformer('all-mpnet-base-v2', device='cuda' if torch.cuda.is_available() else "cpu")
# model = SentenceTransformer('all-MiniLM-L6-v2', device='cuda' if torch.cuda.is_available() else "cpu")


question_answerer = pipeline("question-answering",
                             model='distilbert-base-cased-distilled-squad',
                             device=0 if torch.cuda.is_available() else -1)


summarizer = pipeline("summarization",
                      model="sshleifer/distilbart-cnn-12-6",
                      device=0 if torch.cuda.is_available() else -1)


def get_similar(element, items):
    if len(items) == 0 or len(element) == 0:
        return []
    ready_items = []
    for it in items:
        if it[:2].isnumeric():
            ready_items.append(it[3:])
        else:
            ready_items.append(it)
    emb_element = model.encode(element)
    emb_items = model.encode(ready_items)
    result = []
    scores = cos_sim(emb_element, emb_items)
    maximus = torch.max(scores, 1)
    m = float(maximus.values[0])
    i = int(maximus.indices[0])
    if m > config.threshold:
        result = [round(m, 4), items[i]]
    return result


def get_similar_list(item, items, num):
    if len(items) == 0 or len(item) == 0:
        return [[0, ""]]
    ready_items = []
    for it in items:
        if it[:2].isnumeric():
            ready_items.append(it[3:])
        else:
            ready_items.append(it)
    embeddings = model.encode(ready_items)
    emb_question = model.encode(item)
    scores = cos_sim(emb_question, embeddings)
    scored_texts = []
    for i, score in enumerate(scores[0]):
        scored_texts.append((round(score.item(), 4), items[i]))
    sorted_scored_texts = sorted(scored_texts, key=lambda x: x[0], reverse=True)
    return sorted_scored_texts[:num]
