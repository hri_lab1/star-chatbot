import random
import open_domain_module
import skills_query
import language
import course_recommendation

db = skills_query.QueryMysql()

entities = ["abilities", "knowledge", "skills", "work activities", "ability", "skill", "work activity",
            "tech-skill", "tool", "tech-skills", "tools", "task", "tasks"]
keywords = ["abilities", "knowledge", "skills", "work_activities", "abilities", "skills", "work_activities",
            "tech_skills", "tools_used", "tech_skills", "tools_used", "tasks", "tasks"]


# get data intersection for "compare" questions
def get_intersection(db_data):
    sim = []
    keys = ["abilities", "knowledge", "skills", "work_activities"]
    result = {}
    job1 = db_data[0]
    job2 = db_data[1]
    tools1 = set(job1.get("tools_used"))
    tools2 = set(job2.get("tools_used"))
    result["common_tools"] = list(tools1.intersection(tools2))
    max_tools = max(len(tools1), len(tools2)) if max(len(tools1), len(tools2)) > 0 else 1
    min_tools = min(len(tools1), len(tools2))
    result["perc_tools"] = round(len(result["common_tools"]) / max_tools, 2)
    sim.append(result["perc_tools"])
    tech1 = set(job1.get("tech_skills"))
    tech2 = set(job2.get("tech_skills"))
    result["common_tech_skills"] = list(tech1.intersection(tech2))
    max_tech = max(len(tech1), len(tech2)) if max(len(tech1), len(tech2)) > 0 else 1
    min_tech = min(len(tech1), len(tech2))
    result["perc_tech_skills"] = round(len(result["common_tech_skills"]) / max_tech, 2)
    sim.append(result["perc_tech_skills"])
    task1 = set([x[1] for x in job1.get("tasks")])
    task2 = set([x[1] for x in job2.get("tasks")])
    result["common_tasks"] = list(task1.intersection(task2))
    max_tasks = max(len(task1), len(task2)) if max(len(task1), len(task2)) > 0 else 1
    min_tasks = min(len(task1), len(task2))
    result["perc_tasks"] = round(len(result["common_tasks"]) / max_tasks, 2)
    sim.append(result["perc_tasks"])
    sim2 = []
    all1 = []
    all2 = []
    total_len = min_tools + min_tech + min_tasks
    for k in keys:
        ent1 = job1.get(k)
        ent2 = job2.get(k)
        all1 += [[x[0],x[1],x[2]] for x in ent1]
        all2 += [[x[0],x[1],x[2]] for x in ent2]
        diff_ent = []
        tot_diff = 0
        max_diff = len(ent1) * 5
        total_len += min(len(ent1), len(ent2))
        if len(ent1) != len(ent2) or len(ent1) == 0 or len(ent2) == 0:
            continue
        for i, element in enumerate(ent1):
            n_diff = abs(element[0] - ent2[i][0])
            diff = [n_diff, element[1], element[2]]
            diff_ent.append(diff)
            tot_diff += n_diff
        result[k] = sorted(diff_ent, key=lambda x: x[0], reverse=False)
        result[k + "_tot_diff"] = tot_diff
        result[k + "_perc_sim"] = 1 - round(tot_diff / max_diff, 2)
        sim.append(result[k + "_perc_sim"])
        sim2.append(result[k + "_perc_sim"])
    result["tot_perc_sim"] = round(sum(sim) / len(sim), 2)
    if len(sim2) > 0:
        result["scored_perc_sim"] = round(sum(sim2) / len(sim2), 2)
    result["no_data_flag"] = total_len == 0
    # print(all1)
    result["job1_all"] = sorted(all1 ,key=lambda x: x[0], reverse=True)[:5]
    result["job2_all"] = sorted(all2, key=lambda x: x[0], reverse=True)[:5]
    # print(result)
    return result


def clean_data(data):
    keys = ["abilities", "knowledge", "skills", "work_activities"]
    keys2 = ["tech_skills", "tools_used"]
    c_data = {"job": data.get("job")[0][1], "job_code": data.get("job")[0][0]}
    for k in keys:
        if data.get(k) is not None:
            c_data[k] = [[float(x[1]), x[2], x[3]] for x in data.get(k)]
    for k in keys2:
        if data.get(k) is not None:
            c_data[k] = [x[1] for x in data.get(k)]
    c_data["tasks"] = [[float(x[1]), x[3]] for x in data.get("tasks")]
    return c_data


def query_type_0(data):
    selected_question = data.get("selected_question")[1]
    answer = language.free_questions.get(selected_question)
    if answer is not None:
        if isinstance(answer, list):
            answer = answer[int(random.randint(0, len(answer) - 1))]
        data["answer"] = answer
    return data


def query_type_1(data):
    ins = data.get("instances")[0]
    alt_names = ins.get("alternative_names")
    name = ins.get("name")
    if name in alt_names:
        alt_names.remove(name)
    data["answer"] = "Some alternative names for {job} are: {names}".format(job=name, names=", ".join(alt_names[:10]))
    return data


def query_type_2(data):
    selected_question = data.get("selected_question")[1]
    num = data.get("numbers")
    n = 5 if len(num) == 0 else int(num[0])
    ins = data.get("instances")[0]
    job_code = ins.get("job_code")
    job_name = ins.get("name")
    # print("job_code:", job_code)
    db_data = db.get_all_skills(job_code)
    # print(db_data)
    key = ""
    ind = -1
    for i, ent in enumerate(entities):
        if " " + ent + " " in selected_question:
            key = keywords[i]
            ind = i
            break
    if key != "":
        ent_values = db_data.get(key)
        if 0 <= ind < 7:
            values = [[float(x[1]), x[2], x[3]] for x in ent_values]
            if len(values) > 1 and n > 1:
                sorted_values = sorted(values, key=lambda x: x[0], reverse=True)
                data["answer"] = "I think they are: {ent}".format(ent=" - ".join([x[1] for x in sorted_values[:n]]))
            elif len(values) == 1 or n == 1:
                sorted_values = sorted(values, key=lambda x: x[0], reverse=True)
                data["answer"] = "I think it is: {ent}".format(ent=" - ".join([x[1] for x in sorted_values[:n]]))
            else:
                data["answer"] = "Sorry there is no data about {key} and {name}".format(key=key, name=job_name)
        elif ind > 10:
            values = [[float(x[1]), x[3]] for x in ent_values]
            if len(values) > 1 and n > 1:
                sorted_values = sorted(values, key=lambda x: x[0], reverse=True)
                data["answer"] = "I think they are: {ent}".format(ent=" - ".join([x[1] for x in sorted_values[:n]]))
            elif len(values) == 1 or n == 1:
                sorted_values = sorted(values, key=lambda x: x[0], reverse=True)
                data["answer"] = "I think it is: {ent}".format(ent=" - ".join([x[1] for x in sorted_values[:n]]))
            else:
                data["answer"] = "Sorry there is no data about {key} and {name}".format(key=key, name=job_name)
        else:
            values = [x[1] for x in ent_values]
            if len(values) > 0:
                sorted_values = sorted(values, reverse=False)
                ans = "There are {tot} {key} for {name}. "
                if 1 < n < len(values):
                    ans += "The first {num} in alphabetical order are: {ent}"
                elif 1 == n < len(values):
                    ans += "The first one in alphabetical order is: {ent}"
                elif 1 == n == len(values):
                    ans += "It is: {ent}"
                else:
                    ans += "They are (in alphabetical order): {ent}"
                data["answer"] = ans.format(ent=" - ".join(sorted_values[:n]),
                                            tot=len(values), key=key, num=n, name=job_name)
            else:
                data["answer"] = "Sorry there is no data about {key} and {name}".format(key=key, name=job_name)
    return data


def query_type_3(data):
    instances = data.get("instances")
    if instances is not None and len(instances) > 0:
        for ins in instances:
            if ins.get("entity") == "jobs":
                job_code = ins.get("job_code")
                job = db.get_job_description(job_code)[0]
                data["answer"] = job[2]
    return data


def query_type_4(data):
    instances = [x for x in data.get("instances") if x.get("entity") == "jobs"]
    if len(instances) < 2:
        return default_query_type(data)

    ins1 = data.get("instances")[0]
    job_code1 = ins1.get("job_code")
    job_name1 = ins1.get("name")
    db_data1 = db.get_all_skills(job_code1)
    db_data1 = clean_data(db_data1)

    ins2 = data.get("instances")[1]
    job_code2 = ins2.get("job_code")
    job_name2 = ins2.get("name")
    db_data2 = db.get_all_skills(job_code2)
    db_data2 = clean_data(db_data2)

    result = get_intersection([db_data1, db_data2])
    # print(result)
    if result.get("no_data_flag"):
        answer = "I'm sorry but there is not enough data yet to answer your question. But from the Internet: \n"
        open_domain_answer, data["url"] = open_domain_module.open_domain(" ".join(data.get("sentences")))
        answer += open_domain_answer
    else:
        answer = job_name1 + " and " + job_name2 + " have: "
        n = len(result.get("common_tools"))
        answer += str(n) + " (" + str(round(result.get("perc_tools") * 100, 2)) + "%)"
        answer += " common tool" + ("s" if n>1 else "")
        if n > 5:
            answer += " (first 5 items in alphabetical order: "
        elif 0 < n <= 5:
            answer += " ("
        if n > 0:
            answer += " - ".join(sorted(result.get("common_tools")[:5])) + ")"

        n = len(result.get("common_tech_skills"))
        answer += ", " + str(n) + " (" + str(round(result.get("perc_tech_skills") * 100, 2)) + "%)"
        answer += " common tech-skill" + ("s" if n>1 else "")
        if n > 5:
            answer += " (first 5 items in alphabetical order: "
        elif 0 < n <= 5:
            answer += " ("
        if n > 0:
            answer += " - ".join(sorted(result.get("common_tech_skills")[:5])) + ")"

        n = len(result.get("common_tasks"))
        answer += ", and " + str(n) +" (" + str(result.get("perc_tasks") * 100) + "%)"
        answer += " common task" + ("s" if n>1 else "")
        if n > 5:
            answer += " (first 5 items in alphabetical order: "
        elif 0 < n <= 5:
            answer += " ("
        if n > 0:
            answer += " - " + " - ".join(sorted(result.get("common_tasks")[:5]))+ ")"

        answer += ". "
        ab = result.get("abilities")
        kn = result.get("knowledge")
        sk = result.get("skills")
        wa = result.get("work_activities")
        if ab is not None or kn is not None or sk is not None or wa is not None:
            answer += "They also have: "
            if ab is not None:
                ab = [x[1] for x in result.get("abilities")]
                answer += str(round(result.get("abilities_perc_sim") * 100, 2))
                answer += "% ability similarity (first 5 items sorted by least difference: "
                answer += " - ".join(ab[:5]) + "), "
            if kn is not None:
                kn = [x[1] for x in result.get("knowledge")]
                answer += str(round(result.get("knowledge_perc_sim") * 100, 2))
                answer += "% knowledge similarity (first 5 items sorted by least difference: "
                answer += " - ".join(kn[:5]) + "), "
            if sk is not None:
                sk = [x[1] for x in result.get("skills")]
                answer += str(round(result.get("skills_perc_sim") * 100, 2))
                answer += "% skill similarity (first 5 items sorted by least difference: "
                answer += " - ".join(sk[:5]) + "), and "
            if wa is not None:
                wa = [x[1] for x in result.get("work_activities")]
                answer += str(round(result.get("work_activities_perc_sim") * 100, 2))
                answer += "% work-activity similarity (first 5 items sorted by least difference: "
                answer += " - ".join(wa[:5]) + "). "
            answer += str(round(result.get("scored_perc_sim") * 100, 2)) + "% total similarity for scored entities"
            answer += " (abilities, knowledge, skills, work activities) and "
        answer += str(round(result.get("tot_perc_sim") * 100, 2)) + "% global similarity score."
        all1 = result.get("job1_all")
        all2 = result.get("job2_all")
        if all1 is not None and len(all1) > 0:
            answer += " The elements that most characterize the first job (" + job_name1 + ") are: "
            answer += " - ".join([x[1] for x in all1]) + ". "
        if all2 is not None and len(all2) > 0:
            answer += " The elements that most characterize the second job (" + job_name2 + ") are: "
            answer += " - ".join([x[1] for x in all2]) + ". "
    data["answer"] = answer
    return data


def query_type_5(data):
    instances = data.get("instances")
    answer = ""
    order = "in alphabetical order"
    if instances is not None and len(instances) > 0:
        ins = instances[0]
        entity = ins.get("name")
        entity_type = ins.get("entity")
        num = data.get("numbers")
        n = len(num)
        if n > 0:
            num = int(num[0])
        else:
            num = 5
        if entity_type == "tools":
            jobs = db.get_jobs_per_tool_used(entity)

        elif entity_type == "tasks":
            jobs = db.get_jobs_per_task(entity)

        elif entity_type == "tech_skills":
            jobs = db.get_jobs_per_tech_skill(entity)

        else:
            jobs = db.get_jobs_per_entity(entity, entity_type)
            order = "sorted by importance"

        if 1 < len(jobs) < num:
            answer += 'There are only {job} jobs requiring "{en}". '.format(**{"job": len(jobs), "en": entity})
            answer += "Here they are {order}: ".format(order=order, num=num)
        elif len(jobs) > num > 1:
            answer += 'There are {job} jobs requiring "{en}". '.format(**{"job": len(jobs), "en": entity})
            answer += "The top {num} are {order}: ".format(order=order, num=num)
        elif len(jobs) > num == 1:
            answer += 'There are {job} jobs requiring "{en}". '.format(**{"job": len(jobs), "en": entity})
            answer += "The top one is {order}: ".format(order=order, num=num)
        elif len(jobs) == num > 1:
            answer += 'There are exactly {job} jobs requiring "{en}". '.format(**{"job": len(jobs), "en": entity})
            answer += "They are: "
        elif len(jobs) == 1:
            answer += 'There is only one job requiring "{en}": '.format(**{"en": entity})
        else:
            answer += 'There is no job requiring "{en}". '.format(**{"en": entity})
        if len(jobs) > 0:
            answer += " - ".join(jobs[:num]) + ". ".replace("..", ".").replace("--", "-")
        data["answer"] = answer

    return data


def query_type_6(data):
    instances = data.get("instances")
    answer = ""
    if instances is not None and len(instances) > 0:
        ins = instances[0]
        entity = ins.get("name")
        entity_type = ins.get("entity")
        if entity_type == "tools":
            jobs = db.get_jobs_per_tool_used(entity)
        elif entity_type == "tasks":
            jobs = db.get_jobs_per_task(entity)
        elif entity_type == "tech_skills":
            jobs = db.get_jobs_per_tech_skill(entity)
        else:
            jobs = db.get_jobs_per_entity(entity, entity_type)
        if len(jobs) > 1:
            answer += "There are {job} jobs requiring {en}.".format(**{"job": len(jobs), "en": entity})
        elif len(jobs) == 1:
            answer += "There is only one job requiring {en}.".format(**{"en": entity})
        else:
            answer += "There is no job requiring {en}.".format(**{"en": entity})
        data["answer"] = answer
    return data


def query_type_7(data):
    instances = data.get("instances")
    if instances is not None and len(instances) > 0:
        ins = instances[0]
        entity = ins.get("name")
        answer = course_recommendation.recommend(entity)
    else:
        answer = course_recommendation.recommend(" ".join(data.get("sentences")))
    # print(answer)
    if answer.get("result") is not None and answer.get("result") == "ok":
        nl_answer = "May I suggest a course from {source}, organized by {org}, and entitled: {title}."
        nl_answer +=" You can find more information by visiting the related url: {url}"
        data["answer"] = nl_answer.format(**answer)
        data["url"] = answer.get("url")
    else:
        default_query_type(data)
    return data


def default_query_type(data):
    answer, url = open_domain_module.open_domain(" ".join(data.get("sentences")))
    data["answer"] = "Sorry, I don't know the answer to your question, yet. But from the Internet: \n" + answer
    data["url"] = url
    return data


functions = [query_type_0,
             query_type_1,
             query_type_2,
             query_type_3,
             query_type_4,
             query_type_5,
             query_type_6,
             query_type_7,
             ]


# which jobs require desktop computers?
# which jobs require "Preside over, or serve on, boards of directors, management committees, or other governing boards."
# count occupations needing written comprehension ability
# for what pilot and taxi driver are similar?
# what are the most important abilities for taxi drivers
# which jobs require adobe reader
# What are the main work activities for the occupation taxi driver
# What are the main tech-skills for the occupation taxi driver
# for what "taxi driver" and "data scientist" are similar?
# tell me the differences between Range Managers and Park Naturalists
# tell me the differences between Civil Engineer and Mechanic Engineer
# compare developers to engineers
# recommend me something on IOT
