var c_heigth;
var p_heigth;
var normal_adjust = setInterval(adjust, 1000);
var position = [0, 0];
var settings_visible = false
var help_visible = false
var reduced = false
var spinner = '<div id="bot_spinner"><p class="bot_spinner"><img src="spinner.gif" alt="working..." width="50" height="50"></p></div>'
var current_answer;
var div_heigth = 210;
var pad = 20;
var active = 0;
var entities;
var page_restart = false;
var help_added = false;
var completed = false;
var vsi_activated = false;

var interview_cont = [
    "Let's continue the interview.<br>",
    "Let's proceed with the interview.<br>",
    "Let's pick up where we left off in the interview.<br>",
    "Let's carry on with the interview.<br>",
    "Let's resume the interview.<br>"
]

var bot_conversation = [
    "If using speech recognition, everything you say until you say \"next question\" will be considered the answer to the current question. The first question is: What is your degree?",
    "What are your knowledge?",
    "What are your main abilities?",
    "What work activities have you experienced?",
    "What are you best skills?",
    "Which tools do you know best?",
    "What work tasks are you most skilled at?",
    "The last question is: What are your best tech skills?",
    "Now I analyze your CV... See your results!"
];

var helps = [
    "null",
    "knowledge",
    "abilities",
    "work_activities",
    "skills",
    "tools_used",
    "tasks",
    "tech_skills",
    "null"
]

var helps_pos = [
    -1,
    2,
    2,
    2,
    2,
    1,
    3,
    1,
    -1
]

var cv_add = [
    "Education: ",
    "Knowledge: ",
    "Main abilities: ",
    "Experienced work activities: ",
    "Best skills: ",
    "Better known tools: ",
    "More skilled at tasks such as: ",
    "Best tech skills: "
];

var job_request = "First of all, which job category would you like to apply for? <br>(You can answer here or select it directly on the page)";

var int_next = ["next question", "next"];
var yes = ["yes", "y", "yeah", "Yes", "Y", "YES", "Yeah", "YEAH"]

var interview_status = 0;
var job_ok = 0;
var paused = false;
var vsi_button = document.getElementById('vsi_button')
var yet_paused = false;



// Make the DIV elements draggable:
dragElement(document.getElementById("starbot"));

function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
        // if present, the header is where you move the DIV from:
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
        // otherwise, move the DIV from anywhere inside the DIV:
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        div_resize()
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

var getJSON = function (url, callback) {
    xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};


function user(answer) {
    msg = '<div class="user_input"><img class="user" src="user.png" alt="user" ><p class="dialogue_text">';
    msg += answer + '</p></div>';
    var dialogue = document.getElementById("dialogue");
    dialogue.innerHTML += msg;
    adjust(true)
}

function bot(answer) {
    msg = '<div class="starbot"><img class="bot" src="bot.png" alt="Starbot"><p class="dialogue_text">';
    msg += answer + '</p></div>';
    var dialogue = document.getElementById("dialogue");
    dialogue.innerHTML += msg;
    adjust(true)
    if (speaker) {
        var to_say = answer.split(". ");
        for (phrase of to_say) {
            if (phrase.indexOf("http") < 0) {
                say(phrase);
            }
        }
    }
}

function switch_spinner(mode) {
    var dialogue = document.getElementById("dialogue");
    if (mode) {
        current_answer = dialogue.innerHTML;
        dialogue.innerHTML += spinner;
    } else {
        dialogue.innerHTML = current_answer;
    }
    adjust(true)
}


function adjust(force) {
    let m;
    if (active || !vsi_activated) {
        m = 1
    }
    var local_c_heigth = document.getElementById("dialogue").clientHeight;
    var local_p_heigth = document.getElementById("starbot").clientHeight;
    var dialogue = document.getElementById('dialogue');
    dialogue.style.maxHeight = (p_heigth - 105 * m) + "px";
    document.getElementById('help_div').style.maxHeight = (p_heigth - 95) + "px";
    document.getElementById('settings').style.maxHeight = (p_heigth - 95) + "px";

    if (local_c_heigth != c_heigth || local_p_heigth != p_heigth || force) {
        c_heigth = local_c_heigth;
        p_heigth = local_p_heigth;
        dialogue.scrollBy({
            top: 1000,
            behavior: 'smooth'
        });
    }
}


cycle = function () {
    if (vsi_activated) {
        cycle_vsi();
    } else {
        cycle_bot();
    }
}


cycle_bot = function () {

    var input_text = document.getElementById("user_input");

    var query = input_text.value;
    if (query.length > 0) {
        user(query);
        input_text.value = "";
        switch_spinner(true)
        getJSON('/starbot/?query=' + encodeURIComponent(query), function (err, data) {

            let answer;
            if (err !== null) {
                answer = "I'm sorry, but there was an error. Can you try another question?";
            } else {
                answer = data.answer
                if (data.url && answer.indexOf(data.url) > -1) {
                    answer = answer.replace(data.url, `<a target="_blank" href="${data.url}"> link </a>`)
                }
                if (data.url && (data.question_type == "99" || answer.indexOf(data.url) < 0)) {
                    answer = answer + ". For further details see: " + '<a target="_blank" href="' + data.url + '"> link </a>'
                    answer = answer.replace(".. ", ". ").replace(". .", ".")
                }
            }
            switch_spinner(false)
            bot(answer);
        });
    }

}

cycle_vsi = function () {

    var input_text = document.getElementById("user_input");
    var cv_text = document.getElementById("txt");
    var job = document.getElementById("job");
    var query = input_text.value;

    if (job_ok == 0 && query.length > 0) {
        user(query);
        input_text.value = "";
        let user_job = get_job(query);
        if (user_job.length > 0) {
            bot("Is " + user_job + " your choice?")
            job_ok = 1;
            job.value = user_job;
        } else {
            bot("Your choice is not available. Please retry.")
        }

    } else if (job_ok == 1 && query.length > 0) {
        user(query);
        if (yes.indexOf(query.toLowerCase()) > -1) {
            job_ok = 2;

            input_text.value = "";
            bot(bot_conversation[interview_status])
            interview_status += 1;
            get_entities();
            return

        } else {
            job_ok = 0;
            input_text.value = "";

            bot("Please retry. Which job category would you like to apply for?")
        }
    }

    if (interview_status == 0) {
        input_text.value = "";
        return
    }

    if (query.length > 0) {
        user(query);
        input_text.value = "";
        if (recon.listening) {
            if (interview_status == 1 && !help_added) {
                cv_text.value += cv_add[interview_status - 1]
                help_added = true;
            }
            if (int_next.indexOf(query.toLowerCase()) > -1 && interview_status <= bot_conversation.length - 1 && !completed) {
                let answer = bot_conversation[interview_status];
                if (entities[helps[interview_status]] && entities[helps[interview_status]].length >= 3) {
                    answer += "<br>(like: <br>" + entities[helps[interview_status]][0][helps_pos[interview_status]]
                        + " <br> " + entities[helps[interview_status]][1][helps_pos[interview_status]]
                        + " <br> " + entities[helps[interview_status]][2][helps_pos[interview_status]]
                        + ")";
                }
                bot(answer);
                interview_status += 1;
                if (cv_add[interview_status - 1]) {
                    cv_text.value += "\n" + cv_add[interview_status - 1]
                }

                if (interview_status == bot_conversation.length) {
                    completed = true;
                    suggest();
                    const myTimeout = setTimeout(restart, 3000);
                    const myTimeout2 = setTimeout(reduce, 3100);
                }
            } else if (interview_status <= bot_conversation.length - 1 && !completed) {

                bot("Please continue...")
                cv_text.value += " " + query;

            }
        } else {
            if (interview_status <= bot_conversation.length - 2) {

                cv_text.value += cv_add[interview_status - 1] + query + "\n";
                let answer = bot_conversation[interview_status];
                if (entities[helps[interview_status]] && entities[helps[interview_status]].length >= 3) {
                    answer += "<br>(like: <br>" + entities[helps[interview_status]][0][helps_pos[interview_status]]
                        + " <br> " + entities[helps[interview_status]][1][helps_pos[interview_status]]
                        + " <br> " + entities[helps[interview_status]][2][helps_pos[interview_status]]
                        + ")";
                }
                bot(answer);
                interview_status += 1;
            } else if (interview_status == bot_conversation.length - 1) {
                cv_text.value += cv_add[interview_status - 1] + query + "\n";
                bot(bot_conversation[interview_status]);
                interview_status += 1;
                suggest();
                const myTimeout = setTimeout(restart, 3000);
                const myTimeout2 = setTimeout(reduce, 3100);
                page_restart = true;
                vsi_button.innerHTML = "START";
                vsi_button.title = "Start the interview";
            }
        }

    }


}

function vsi() {
    //console.log(vsi_activated)
    vsi_activated = !vsi_activated;
    if (page_restart) {
        //document.getElementById('txt').value = "";
        //document.getElementById('job').value = "";
        //let container = document.getElementById('container')
        //let container2 = document.getElementById('container2')
        let result = document.getElementById('result')
        //container.style.display = "none";
        //container2.style.display = "none";
        //result.style.display = "none";
        result.innerHTML = "";
        update_select("");
        page_restart = false;
        help_added = false;
        completed = false;
        interview_status = 0;
        job_ok = 0;
        yet_paused = false;
    }

    if (vsi_activated && interview_status == 0) {
        var job = document.getElementById("job");
        interview_status = 0;
        active = 1;
        //document.getElementById('vsi_button').style.display = "none";
        vsi_button.innerHTML = "PAUSE"
        vsi_button.title = "Pause the interview";
        if (reduced) {
            enlarge();
        }

        div_heigth = 425;
        initial_position()
        //document.getElementById('starbotfooter').style.display = "flex";
        //document.getElementById('dialogue').innerHTML = "";
        if (job.value.length > 0 && job_list.indexOf(job.value) > -1) {
            get_entities();
            job_ok = 2;
            bot("Let's start the interview. " + bot_conversation[interview_status]);
            interview_status += 1;
        } else {
            if(yet_paused){
                bot(interview_cont[Math.floor(Math.random()*interview_cont.length)] + job_request);
            } else{
                bot("Let's start the interview. " + job_request);
            }
            
        }

    } else if (!vsi_activated) {
        vsi_button.innerHTML = "CONTINUE"
        vsi_button.title = "Continue the interview";
        bot("What can I do for you?")
        yet_paused = true;
    } else if (vsi_activated) {
        vsi_button.innerHTML = "PAUSE";
        vsi_button.title = "Pause the interview";
        interview_status -= 1;
        bot(interview_cont[Math.floor(Math.random()*interview_cont.length)] + bot_conversation[interview_status]);
        interview_status += 1;
        yet_paused = true;
    } 


}

function reduce() {
    var bot_div = document.getElementById('starbot');
    var h = window.innerHeight;
    let top, left;
    if (bot_div.style.top) {
        top = bot_div.style.top;
    } else {
        top = "400px"
    }

    if (bot_div.style.left) {
        left = bot_div.style.left;
    } else {
        left = "250px"
    }

    position = [top, left]

    //var starbot_header = document.getElementById('starbotheader');
    var starbot_footer = document.getElementById('starbotfooter');
    var dialogue = document.getElementById('dialogue');
    document.getElementById('enlarge').style.display = "flex";
    document.getElementById('reduce').style.display = "none";
    document.getElementById('show_settings').style.display = "none";
    document.getElementById('help').style.display = "none";
    //starbot_header.style.display = "none";
    dialogue.style.display = "none"
    starbot_footer.style.display = "none"
    bot_div.style.minHeight = "60px";
    bot_div.style.maxHeight = "60px";
    bot_div.style.maxWidth = "215px";
    bot_div.style.resize = "none";

    let dims = get_dims();
    var bot_div = document.getElementById('starbot');
    let element = { top: bot_div.offsetTop, left: bot_div.offsetLeft, height: bot_div.offsetHeight, width: bot_div.offsetWidth }

    element.top = dims.height - element.height / 2 - pad;
    element.left = dims.width - element.width / 2 - pad;

    bot_div.style.top = element.top + "px"
    bot_div.style.left = element.left + "px"
    reduced = true;

}

function enlarge() {
    var bot_div = document.getElementById('starbot');
    //bot_div.style.top = position[0];
    //bot_div.style.left = position[1];
    var dialogue = document.getElementById('dialogue');
    var starbot_footer = document.getElementById('starbotfooter');
    bot_div.style.removeProperty('min-height');
    bot_div.style.removeProperty('max-height');
    bot_div.style.removeProperty('max-width');
    bot_div.style.removeProperty('resize');
    document.getElementById('enlarge').style.display = "none";
    document.getElementById('reduce').style.removeProperty('display');
    document.getElementById('show_settings').style.removeProperty('display');
    document.getElementById('help').style.removeProperty('display');
    dialogue.style.removeProperty('display');
    starbot_footer.style.removeProperty('display');
    reduced = false
    //div_resize();
    initial_position()
}

function settings() {
    var settings_div = document.getElementById('settings');
    var dialogue = document.getElementById('dialogue');
    if (settings_visible) {
        settings_div.style.display = "none";
        dialogue.style.removeProperty('display');
        document.getElementById('show_settings').src = 'settings.png';
        document.getElementById('restart').style.removeProperty('display');
        document.getElementById('reduce').style.removeProperty('display');
        document.getElementById('starbotfooter').style.removeProperty('display');
        document.getElementById('help').style.removeProperty('display');
        var new_activation = document.getElementById('activation').value;
        if (activation.indexOf(new_activation) < 0) {
            activation.push(new_activation)
            update_grammars();
        }
    } else {
        settings_div.style.display = "block";
        dialogue.style.display = "none";
        document.getElementById('show_settings').src = 'no_settings.png';
        document.getElementById('restart').style.display = "none";
        document.getElementById('reduce').style.display = "none";
        document.getElementById('starbotfooter').style.display = "none";
        document.getElementById('help').style.display = "none";
    }
    settings_visible = !settings_visible
}

function help() {
    var help_div = document.getElementById('help_div');
    var dialogue = document.getElementById('dialogue');
    if (help_visible) {
        help_div.style.display = "none";
        dialogue.style.removeProperty('display');
        document.getElementById('help').src = 'help.svg';
        document.getElementById('restart').style.removeProperty('display');
        document.getElementById('reduce').style.removeProperty('display');
        document.getElementById('starbotfooter').style.removeProperty('display');
        document.getElementById('show_settings').style.removeProperty('display');
    } else {
        help_div.style.display = "block";
        dialogue.style.display = "none";
        document.getElementById('help').src = 'no_help.svg';
        document.getElementById('restart').style.display = "none";
        document.getElementById('reduce').style.display = "none";
        document.getElementById('starbotfooter').style.display = "none";
        document.getElementById('show_settings').style.display = "none";
    }
    help_visible = !help_visible
}

function restart() {
    vsi_activated = false;
    active = 0
    div_heigth = 250;
    //document.getElementById('dialogue').innerHTML = '<div class="starbot"><img class="bot" src="bot.png" alt="VSI"><p class="dialogue_text">Welcome! I am VSI, the Virtual STAR interviewer. I am here to help you fill out your resume. Click the VSI button when you are ready to begin the interview. Otherwise, you can write your resume in the "Input CV" box or upload a TXT or a PDF file.</p></div>';
    //document.getElementById('dialogue').innerHTML = '<div class="starbot"><img class="bot" src="bot.png" alt="Starbot"><p class="dialogue_text">Welcome! I\'m StarBot, your personal assistant. What can I do for you?</p></div>';
    document.getElementById('dialogue').innerHTML = '<div class="starbot"><img class="bot" src="bot.png" alt="VSI"><p class="dialogue_text">Welcome! I\'m StarBot, your personal assistant. Click the VSI button when you are ready to begin the interview. In the meantime, you can ask me anything you want to know about the world of work.</p></div>';
    document.getElementById('starbot').style.height = div_heigth + "px";
    //document.getElementById('vsi_button').style.display = "flex";
    vsi_button.innerHTML = "START";
    vsi_activated.title = "Start the interview";
    enlarge();
    initial_position();
}

document.addEventListener("keypress", function (event) {
    //alert(event.code)
    if ((event.code == "Enter" || event.code == "NumpadEnter") && document.getElementById('user_input').value.length > 0) {
        cycle();
    }
});

function get_dims() {
    let width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    let height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    return { "width": width, "height": height }
}

function div_resize() {
    let dims = get_dims();
    var bot_div = document.getElementById('starbot');
    let element = { top: bot_div.offsetTop, left: bot_div.offsetLeft, height: bot_div.offsetHeight, width: bot_div.offsetWidth }
    let h = element.height;

    if (element.top + element.height / 2 + pad > dims.height) {
        element.top = dims.height - element.height / 2 - pad;
    }
    if (element.top - element.height / 2 < pad) {
        element.top = element.height / 2 + pad;
    }

    if (element.left + element.width / 2 + pad > dims.width) {
        element.left = dims.width - element.width / 2 - pad;
    }
    if (element.left - element.width / 2 < pad) {
        element.left = element.width / 2 + pad;
    }

    if (element.height + 2 * pad > dims.height) {
        element.height = dims.height - 2 * pad;
    }

    bot_div.style.left = element.left + "px";
    bot_div.style.top = element.top + "px";

    if (h > element.height) {
        bot_div.style.height = element.height + "px";
    }

    adjust(false);

    if (interview_status == 0 && vsi_activated && job_ok == 0) {
        seek_job()
    }

}

function initial_position() {
    let dims = get_dims();
    var bot_div = document.getElementById('starbot');
    let element = { top: bot_div.offsetTop, left: bot_div.offsetLeft, height: bot_div.offsetHeight, width: bot_div.offsetWidth }

    bot_div.style.minHeight = 250 + "px";
    bot_div.style.height = div_heigth + "px";
    bot_div.style.width = 300 + "px";

    element.top = dims.height - element.height / 2 - pad;
    element.left = dims.width - element.width / 2 - pad;

    bot_div.style.top = element.top + "px";
    bot_div.style.left = element.left + "px";
}


function get_job(job_search) {
    let found_jobs = []
    let found_job;

    for (i in job_list) {
        j = job_list[i];
        if (j.toLowerCase().includes(job_search.toLowerCase())) {
            found_jobs.push(j)
        }
    }
    if (found_jobs.length > 0) {
        found_job = fuzzy_search(found_jobs, job_search, 0.85);
    } else {
        found_job = fuzzy_search(job_list, job_search, 0.85);
    }
    return found_job;
}

function get_entities() {
    var job = document.getElementById("job");

    getJSON('/all_skills/?job=' + encodeURIComponent(job.value.toLowerCase()), function (err, data) {

        let answer;
        if (err !== null) {
            console.log(err)
            entities = [];
        } else {
            entities = data;
            console.log(entities)
        }
    });
}


function speak_test() {
    let test_text = document.getElementById("test_voice").value;
    say(test_text);
}

function seek_job() {
    var job = document.getElementById("job")
    if (job.value && job.value.length > 0 && job_list.indexOf(job.value) > -1) {
        get_entities();
        job_ok = 2;
        //interview_status += 1;
        bot("Your choice is: " + job.value + "!<br>" + bot_conversation[interview_status])
        interview_status += 1;
    }
}


document.getElementsByTagName("BODY")[0].onresize = function () { div_resize() };
const myTimeout = setTimeout(restart, 100);
const myInterval = setInterval(div_resize, 300)


