// recognition status
var recon = { listening: false, activated: false}
var mic_image = 'microphone-on1.svg';
var mic_animation;
var activation = ['Ehi computer', 'Computer', "starbot"]
var cycle;

// speake status
var speaker = false


/**
** Text to Speech part
*/

if (window.speechSynthesis){
	var voiceSelect = document.getElementById('voices') //querySelector('select');
	var synth = window.speechSynthesis;
	var voices = [];

	function populateVoiceList() {
	  voices = synth.getVoices().sort(function (a, b) {
		  const aname = a.name.toUpperCase(), bname = b.name.toUpperCase();
		  if ( aname < bname ) return -1;
		  else if ( aname == bname ) return 0;
		  else return +1;
	  });
	  var selectedIndex = voiceSelect.selectedIndex < 0 ? 0 : voiceSelect.selectedIndex;
	  voiceSelect.innerHTML = '';
	  for(i = 0; i < voices.length ; i++) {
		var option = document.createElement('option');
		option.textContent = voices[i].name + ' (' + voices[i].lang + ')';
		
		if(voices[i].default) {
		  option.textContent += ' -- DEFAULT';
		}
		option.setAttribute('data-lang', voices[i].lang);
		option.setAttribute('data-name', voices[i].name);
		if(voices[i].lang.indexOf('en-')>-1){
			voiceSelect.appendChild(option);
		}
	  }

	  for(i = 0; i < voices.length ; i++) {
		var option = document.createElement('option');
		option.textContent = voices[i].name + ' (' + voices[i].lang + ')';
		
		if(voices[i].default) {
		  option.textContent += ' -- DEFAULT';
		}
		option.setAttribute('data-lang', voices[i].lang);
		option.setAttribute('data-name', voices[i].name);
		if(voices[i].lang.indexOf('it-')>-1){
			voiceSelect.appendChild(option);
		}
	  }

	  voiceSelect.selectedIndex = selectedIndex;
	}

	populateVoiceList();

	if (speechSynthesis.onvoiceschanged !== undefined) {
	  speechSynthesis.onvoiceschanged = populateVoiceList;
	}

	function speak(){
		if (document.getElementById('txt').value.length==0){
			return
		}
		say(document.getElementById('txt').value)
	}

	function say(msg){
		let voice_msg = new SpeechSynthesisUtterance(msg);
		voice_msg.rate=1;
		var selectedOption = voiceSelect.selectedOptions[0].getAttribute('data-name');
		
		for(i = 0; i < voices.length ; i++) {
		  if(voices[i].name === selectedOption) {
			voice_msg.voice = voices[i];
			break;
		  }
		}
		window.speechSynthesis.speak(voice_msg);
	}
}

	/**
	** Speech Recognition part
	*/
if (window.webkitSpeechRecognition || window.SpeechRecognition){
	
	var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
	var recognition = new SpeechRecognition();
	
	var grammars = [
		'#JSGF V1.0; grammar utterances; public <utterance> = ' + activation.join(' | ') + ' ',
		'#JSGF V1.0; grammar utterances; public <utterance> = hello | hi '
	]
	var g_dict = {};
	
	// Start listening process
	function interaction(){
		recon.listening = !recon.listening;
		if(recon.listening){
			document.getElementById('button').src = 'stop_listening.png';
			recogn_start(grammars[0]);
		} else {
			recognition.abort();
			document.getElementById('button').src = 'start_listening.png';
		}
	}

	function recogn_start(grammar) {
		if(!recon.listening){
			recognition.abort();
			return
		}
		
		var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
		var speechRecognitionList = new SpeechGrammarList();
		speechRecognitionList.addFromString(grammar, 1);
		recognition.grammars = speechRecognitionList;
		recognition.continuous = false;
		recognition.lang = document.getElementById("rec_lang").options[document.getElementById("rec_lang").selectedIndex].value;
		recognition.interimResults = false;
		recognition.maxAlternatives = 1;
		
		// listening for user command
		if(recon.activated){
			//document.getElementById('result').innerHTML = '';
			
			// fired on result
			recognition.onresult = function (event) {
				let result = event.results[0][0].transcript;
				let search = fuzzy_search(expand(grammar),result);
				document.getElementById('user_input').value = result;
				cycle();
				//document.getElementById('fuzzy_result').innerHTML = search;
			}
			
			// fired on sound end
			recognition.onend = function() {
				setTimeout(function(){
					recognition.lang = document.getElementById("rec_lang").options[document.getElementById("rec_lang").selectedIndex].value;
					recon.activated = false
					listening();
					recogn_start(grammars[0]);
				}, 300);
			}
			
		} 
		
		// listening for activation words
		else {
			
			// fired on result
			recognition.onresult = function (event) {
				let result = event.results[0][0].transcript;
				let search = fuzzy_search(expand(grammar),result);
				if(activation.indexOf(search) != -1){
					recon.activated = true;
					listening();
				}
			}
						
			// fired on sound end
			recognition.onend = function() {
				setTimeout(function(){
					recognition.lang = document.getElementById("rec_lang").options[document.getElementById("rec_lang").selectedIndex].value;
					if(recon.activated){
						recogn_start(grammars[1]);
					} else {
						recogn_start(grammars[0]);
					}
						
				}, 300);
			}
		}
		
		recognition.start();
		
	}
	
	// animate microphone
	function listening(){
		if(recon.activated){	
			mic_animation=setInterval(function(){
				if (mic_image == 'microphone-on1.svg'){
					mic_image = 'microphone-on2.svg';
				} else {
					mic_image = 'microphone-on1.svg';
				}
				document.getElementById('mic').src = mic_image;
			},300);
		} else {
			clearInterval(mic_animation);
			document.getElementById('mic').src = "microphone-off.svg";
		}
	}

	function update_grammars(){
		grammars = [
			'#JSGF V1.0; grammar utterances; public <utterance> = ' + activation.join(' | ') + ' ',
			'#JSGF V1.0; grammar utterances; public <utterance> = hello | hi '
		]
	}
	
} else {
	document.getElementById('mic').src = "microphone-deactivated.svg";
	document.getElementById('button').style.display = "none";
	function listen(){}
	function interaction(){}
	function update_grammars(){}
}


/**
** Fuzzy search part (using "fuse" JavaScript library)
*/

/**
** returns the closest match to [string] in the [list] with a threshold value of 0.7
*/
function fuzzy_search(list,string){
	let fuse = new Fuse(list, {includeScore: true})
	let result = fuse.search(string)
	if (result.length>0 && result[0].score<0.7){
		return result[0].item
	}
	return ""
}

/**
** extracts the list of possible utterances from the grammar
*/
function expand(grammar){
	l = grammar.split(';');
	l.shift();
	l[0] = l[0].replace(' grammar ','');
	let g = l.shift();
	g_dict[g]={};
	for (let i = 0; i<l.length; i++){
		while(l[i].indexOf(' ') == 0){
			l[i]=l[i].replace(' ','');
		}
		while(l[i].slice(l[i].length-1,l[i].length) == ' '){
			l[i]=l[i].slice(0,l[i].length-1)
		}
		l[i]=l[i].split('=');
		for(let k in l[i]){
			if(l[i][k].slice(0,1) == ' '){
				l[i][k] = l[i][k].slice(1,l[i][k].length)
			}
			if(l[i][k].slice(l[i][k].length-1,l[i][k].lenght) == ' '){
				l[i][k] = l[i][k].slice(0,l[i][k].length-1)
			}
		} 
		let n = l[i][0].indexOf('<')+1
		let n2 = l[i][0].indexOf('>')
		l[i][0] = l[i][0].slice(n,n2)
		if(l[i].length == 1 && l[i].indexOf('') != -1){
			l.splice(i,1);
		}
		if(l[i] && l[i][1] && l[i][1].indexOf('|') != -1){
			l[i][1] = l[i][1].split('|');
			for(let j in l[i][1]){
				if(l[i][1][j].slice(0,1) == ' '){
					l[i][1][j] = l[i][1][j].slice(1,l[i][1][j].length)
				}
				if(l[i][1][j].slice(l[i][1][j].length-1,l[i][1][j].lenght) == ' '){
					l[i][1][j] = l[i][1][j].slice(0,l[i][1][j].length-1)
				}
			} 
		}
		if(l[i] && !Array.isArray(l[i][1])){
			g_dict[g][l[i][0]] = [l[i][1]];
		} else if (l[i]){
			g_dict[g][l[i][0]] = l[i][1];
		}
		
	}
	
	for(key in g_dict[g]){
		while (verify_content(g_dict[g][key])){
			for(let i in g_dict[g][key]){
				let n = g_dict[g][key][i].indexOf('<')+1
				let n2 = g_dict[g][key][i].indexOf('>')
				if(n > 0){
					let k = g_dict[g][key][i].slice(n,n2)
					if(g_dict[g][k]){ 
						for(let s in g_dict[g][k]){
							g_dict[g][key].push(g_dict[g][key][i].replace('<'+k+'>',g_dict[g][k][s]));
						}
					}
					g_dict[g][key].splice(i,1);
				}
			}
		}
	}
	return g_dict[g]['utterance']
}

/**
** check if there are words between <> 
*/
function verify_content(v_list){
	for(let k1 in v_list){
		if (v_list[k1].indexOf('<')>-1){
			return true;
		}
	}
	return false;
}

function speaker_on(){
	var speaker_on = document.getElementById("speaker_on");
	var speaker_off = document.getElementById("speaker_off");
	speaker = true;
	if (window.speechSynthesis.paused){
		window.speechSynthesis.resume()
	}
	speaker_off.style.display = "none";
	speaker_on.style.display = "flex";


}

function speaker_off(){
	var speaker_on = document.getElementById("speaker_on");
	var speaker_off = document.getElementById("speaker_off");
	speaker = false;
	//window.speechSynthesis.pause()
	window.speechSynthesis.cancel();
	speaker_off.style.display = "flex";
	speaker_on.style.display = "none";
}
