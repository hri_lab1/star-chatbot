# STAR ChatBot: a ChatBot to Enhance Your Own CV Using the O*NET Database



## How to use

### Prerequisites:

1) MySQL

2) Python 3


### Installation:

1) Download O*NET Database from https://www.onetcenter.org/database.html#all-files and import it.

2) Install Python MySQL connector: 
    ```pip install mysql-connector-python```

3) Install Torch:

    for GPU -> ```pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu113 ```

    for CPU -> ```pip install torch torchvision torchaudio```

4) Install Sentence Transformers:

    ```pip install sentence-transformers```

5) Install Transformers:

    ```pip install transformers```

6) Install Spacy:
    
    ```pip install -U pip setuptools wheel```

    ```pip install -U spacy```
	
	```python -m spacy download en_core_web_sm```

7) Install Fast API 

    ```pip install "fastapi[all]"```
	
8) Install DuckDuckGo-Search and Words2num

    ```pip install duckduckgo-search```
	
	```pip install words2num```


9) Prepare embeddings for entities in the database by running the utility in the program folder

    ```python utility.py```

10) Set the correct database name, username and password in "config.py" 

	```database_parameters = {"host": '192.168.1.200', "user": 'username', "password": 'password', "database": 'database_name'}```
	
11) Obtain an authentication key for the Udemy API at https://www.udemy.com/developers/ and place it in the course_recommendation.py file in the header variable definition:

```
    header = {
			"Accept": "application/json, text/plain, */*",
			"Authorization": "your authorisation",
			"Content-Type": "application/json;charset=utf-8"
	}
 ```

### Run the program


Run ```uvicorn main:app --host 0.0.0.0 --port 80``` and in a browser open ```http://localhost/first.html```
